const fetch = require('node-fetch');
const csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const fs = require('fs');

var pageData = "";
var page = 0;
var pagesTarget = 106;



const csvWriter = createCsvWriter({
	path: 'out.csv',
	header: [
     {id: 'issuerName', title: 'issuerName'},
     {id: 'productName', title: 'productName'},
     {id: 'productCategory', title: 'productCategory'},
     {id: 'comments', title: 'comments'},
     {id: 'ingredients', title: 'ingredients'},
    ]
});



let settings = { method: "Get" };
var fetcher = function(page){
	var url = "https://powiadomienia.gis.gov.pl/api/public/publicregistry?page="+page+"&size=500000000";
	fetch(url, settings)
	.then(res => res.json())
	.then((json) => {
		console.log(page+" Ready!");
		// pageData = pageData + json._embedded.entryToes;
		csvWriter
  				.writeRecords(json._embedded.entryToes)
 				 .then(()=> console.log('The CSV file was written successfully'));
		return(json._embedded.entryToes);
		
        // do something with JSON
    });

}

const secondFunction = async (i) => {
  
  // do something else here after firstFunction completes
}

async function process(){
for (var i = 0; i <= pagesTarget; i++) {
	await fetcher(i);
}}
process()